﻿using System;
using System.Text;
using static System.Math;

namespace A1i1uyamax
{
    [Serializable]
    public class KAMatrix
    {
        public enum InverseMethod
        {
            ByMinors,
        }

        private double[,] array;
        private int row;
        private int column;

        public int Row => row;
        public int Column => column;

        public double this[int i, int j]
        {
            get => array[i, j];
            set => array[i, j] = value;
        }

        public KAMatrix(int row, int column)
        {
            this.row = row;
            this.column = column;
            array = new double[row, column];
        }

        private KAMatrix(KAMatrix matrix)
        {
            this.row = matrix.row;
            this.column = matrix.column;
            array = new double[row, column];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++)
                    array[i, j] = matrix[i, j];
        }

        public static KAMatrix I(int size)
            => Diag(1.0, size);

        public static KAMatrix Diag(params double[] diagonalElements)
        {
            var result = new KAMatrix(diagonalElements.Length, diagonalElements.Length);
            for (int i = 0; i < diagonalElements.Length; i++)
                result[i, i] = diagonalElements[i];
            return result;
        }

        public static KAMatrix Diag(double value, int size)
        {
            var result = new KAMatrix(size, size);
            for (int i = 0; i < size; i++)
                result[i, i] = value;
            return result;
        }

        public static KAMatrix RotationMatrixX(double angle) => new KAMatrix(3, 3)
        {
            [0, 0] = 1.0,
            [1, 1] = Cos(angle),
            [1, 2] = -Sin(angle),
            [2, 1] = Sin(angle),
            [2, 2] = Cos(angle)
        };

        public static KAMatrix RotationMatrixY(double angle) => new KAMatrix(3, 3)
        {
            [0, 0] = Cos(angle),
            [0, 2] = Sin(angle),
            [1, 1] = 1.0,
            [2, 0] = -Sin(angle),
            [2, 2] = Cos(angle)
        };

        public static KAMatrix RotationMatrixZ(double angle) => new KAMatrix(3, 3)
        {
            [0, 0] = Cos(angle),
            [0, 1] = -Sin(angle),
            [1, 0] = Sin(angle),
            [1, 1] = Cos(angle),
            [2, 2] = 1.0
        };

        public KAMatrix Transpose()
        {
            var result = new KAMatrix(column, row);
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++)
                    result.array[j, i] = array[i, j];
            return result;
        }

        public KAMatrix SelfTranspose()
        {
            array = Transpose().array;
            return this;
        }

        private KAMatrix InverseByMinor()
        {
            double det = Determinant();
            if (det == 0.0)
                throw new Exception("Deteminant equals to zero");

            var result = new KAMatrix(row, column);
            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++)
                    result.array[i, j] = Cofactor(array, i, j) / det;
            return result.Transpose();
        }

        public KAMatrix Inverse(InverseMethod mode)
        {
            switch (mode)
            {
                case InverseMethod.ByMinors:
                    return InverseByMinor();
                default:
                    throw new NotImplementedException();
            }
        }

        public double Determinant()
        {
            if (column != row)
                throw new Exception("This is not square matrix! Can not get determinant.");
            return GetDeterminant(array);
        }

        public override string ToString()
            => ToString(string.Empty);

        public string ToString(string format)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                    sb.AppendFormat("{0}\t", array[i, j]);
                sb.Append("\n");
            }
            return sb.ToString();
        }

        public KAMatrix Clone()
            => new KAMatrix(this);

        public Array ToArray() => (Array)array.Clone();

        private double GetDeterminant(double[,] array)
        {
            int n = (int)Math.Sqrt(array.Length);
            if (n == 1) return array[0, 0];
            double det = 0;
            for (int k = 0; k < n; k++) det += array[0, k] * Cofactor(array, 0, k);
            return det;
        }

        private double Cofactor(double[,] array, int row, int column)
            => Pow(-1, column + row) * GetDeterminant(Minor(array, row, column));

        private double[,] Minor(double[,] array, int row, int column)
        {
            int n = (int)Sqrt(array.Length) - 1;
            double[,] minor = new double[n, n];

            int ic = 0;
            for (int i = 0; i < n; i++)
            {
                if (i == row)
                    continue;
                int jc = 0;
                for (int j = 0; j < n; j++)
                {
                    if (j == column)
                        continue;
                    minor[ic, jc] = array[i, j];
                    ++jc;
                }
                ++ic;
            }
            return minor;
        }

        #region Operators

        public static KAMatrix operator *(KAMatrix matrix, double c)
        {
            if (matrix == null)
                throw new ArgumentNullException(nameof(matrix));

            var result = new KAMatrix(matrix.row, matrix.column);
            for (int i = 0; i < matrix.row; i++)
                for (int j = 0; j < matrix.column; j++)
                    result[i, j] = matrix[i, j] * c;
            return result;
        }

        public static KAMatrix operator *(KAMatrix matrix, int c)
            => matrix * (double)c;

        public static KAMatrix operator *(KAMatrix matrix, float c)
            => matrix * (double)c;

        public static KAMatrix operator *(double c, KAMatrix matrix)
            => matrix * c;

        public static KAMatrix operator *(int c, KAMatrix matrix)
            => matrix * c;

        public static KAMatrix operator *(float c, KAMatrix matrix)
            => matrix * c;

        public static KAMatrix operator /(KAMatrix matrix, double c)
        {
            if (c == 0.0)
                throw new DivideByZeroException(nameof(c));

            if (matrix == null)
                throw new ArgumentNullException(nameof(matrix));

            var result = new KAMatrix(matrix.row, matrix.column);
            for (int i = 0; i < matrix.row; i++)
                for (int j = 0; j < matrix.column; j++)
                    result[i, j] = matrix[i, j] / c;
            return result;
        }

        public static KAMatrix operator /(KAMatrix matrix, int c)
            => matrix / (double)c;

        public static KAMatrix operator /(KAMatrix matrix, float c)
            => matrix / (double)c;

        public static KAMatrix operator +(KAMatrix matrixLeft, KAMatrix matrixRight)
        {
            if (matrixLeft == null)
                throw new ArgumentNullException(nameof(matrixLeft));

            if (matrixRight == null)
                throw new ArgumentNullException(nameof(matrixRight));

            if (matrixLeft.row != matrixRight.row || matrixLeft.column != matrixRight.column)
                throw new Exception($"Addition impossible: {nameof(matrixLeft)} and {nameof(matrixRight)}");

            var result = new KAMatrix(matrixLeft.row, matrixLeft.column);
            for (int i = 0; i < matrixLeft.row; i++)
                for (int j = 0; j < matrixLeft.column; j++)
                    result.array[i, j] = matrixLeft.array[i, j] + matrixRight.array[i, j];
            return result;
        }

        public static KAMatrix operator -(KAMatrix matrixLeft, KAMatrix matrixRight)
        {
            if (matrixLeft == null)
                throw new ArgumentNullException(nameof(matrixLeft));

            if (matrixRight == null)
                throw new ArgumentNullException(nameof(matrixRight));

            if (matrixLeft.row != matrixRight.row || matrixLeft.column != matrixRight.column)
                throw new Exception($"Subtraction impossible: {nameof(matrixLeft)} and {nameof(matrixRight)}");

            var result = new KAMatrix(matrixLeft.row, matrixLeft.column);
            for (int i = 0; i < matrixLeft.row; i++)
                for (int j = 0; j < matrixLeft.column; j++)
                    result.array[i, j] = matrixLeft.array[i, j] - matrixRight.array[i, j];
            return result;
        }

        public static KAMatrix operator *(KAMatrix matrixLeft, KAMatrix matrixRight)
        {
            if (matrixLeft == null)
                throw new ArgumentNullException(nameof(matrixLeft));

            if (matrixRight == null)
                throw new ArgumentNullException(nameof(matrixRight));

            if (matrixLeft.column != matrixRight.row)
                throw new Exception($"Multiplication impossible: {nameof(matrixLeft)} and {nameof(matrixRight)}");

            var result = new KAMatrix(matrixLeft.row, matrixRight.column);
            for (int i = 0; i < matrixLeft.row; i++)
                for (int j = 0; j < matrixRight.column; j++)
                {
                    double sum = 0;
                    for (int k = 0; k < matrixLeft.column; k++)
                        sum += matrixLeft.array[i, k] * matrixRight.array[k, j];
                    result.array[i, j] = sum;
                }
            return result;
        }

        #endregion
    }
}
