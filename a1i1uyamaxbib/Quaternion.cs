﻿using System;
using System.Text;

namespace A1i1uyamax
{
    [Serializable]
    public sealed class KAQuaternion
    {
        public static KAQuaternion Identity = new KAQuaternion();
        public static KAQuaternion NULL = new KAQuaternion(0, 0, 0, 0);

        public KAVector Vector { get; }
        public double W { get; set; }
        public double X
        {
            get => Vector.X;
            set => Vector.X = value;
        }
        public double Y
        {
            get => Vector.Y;
            set => Vector.Y = value;
        }
        public double Z
        {
            get => Vector.Z;
            set => Vector.Z = value;
        }

        public KAQuaternion(KAVector vector, double W) : this(vector)
            => this.W = W;

        public KAQuaternion()
        {
            Vector = new KAVector();
            W = 1.0;
        }

        public KAQuaternion(double X, double Y, double Z, double W)
        {
            Vector = new KAVector(X, Y, Z);
            this.W = W;
        }

        public KAQuaternion(float X, float Y, float Z, float W)
            : this((double)X, (double)Y, (double)Z, (double)W)
        {
        }

        public KAQuaternion(double angle, KAVector vector)
        {
            this.Vector = vector * Math.Sin(angle / 2.0);
            W = Math.Cos(angle / 2.0);
        }

        public KAQuaternion(KAQuaternion quaternion)
        {
            Vector = new KAVector(Vector);
            W = quaternion.W;
        }

        public KAQuaternion(KAVector vector)
            => Vector = vector;

        public override int GetHashCode()
            => X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode() + W.GetHashCode();

        public bool Equals(KAQuaternion quaternion)
            => quaternion != null && Equals(this, quaternion);

        public override bool Equals(object obj)
            => obj is KAQuaternion quaternion && Equals(quaternion);

        public double Arg()
            => Math.Acos(W / Module());

        public double Norm()
            => X * X + Y * Y + Z * Z + W * W;

        public KAQuaternion ToNormal()
            => Module() != 0.0 ? this / Module() : new KAQuaternion();

        public double Module() => Math.Sqrt(Norm());

        public KAQuaternion Conjugate() => new KAQuaternion(-Vector, W);

        public KAQuaternion Inverse() => Conjugate() / Norm();

        public override string ToString()
            => new StringBuilder()
                .Append($"{W} ")
                .Append($"{X} ")
                .Append($"{Y} ")
                .Append(Z)
                .ToString();

        public string ToString(string format)
            => new StringBuilder()
                .AppendFormat(format, $"{W} ")
                .AppendFormat(format, $"{X} ")
                .AppendFormat(format, $"{Y} ")
                .AppendFormat(format, Z)
                .ToString();

        public KAMatrix ToRotateMatrix() => new KAMatrix(3, 3)
        {
            [0, 0] = W * W - 0.5 + Vector.X * Vector.X,
            [0, 1] = Vector.Y * Vector.X + W * Vector.Z,
            [0, 2] = Vector.X * Vector.Z - W * Vector.Y,
            [1, 0] = Vector.X * Vector.Y - W * Vector.Z,
            [1, 1] = W * W - 0.5 + Vector.Y * Vector.Y,
            [1, 2] = Vector.Y * Vector.Z + W * Vector.X,
            [2, 0] = Vector.X * Vector.Z + W * Vector.Y,
            [2, 1] = Vector.Y * Vector.Z - W * Vector.X,
            [2, 2] = W * W - 0.5 + Vector.Z * Vector.Z
        } * 2.0;

        public KAMatrix ToMatrix() => new KAMatrix(4, 1)
        {
            [0, 0] = W,
            [1, 0] = X,
            [2, 0] = Y,
            [3, 0] = Z
        };

        public KAVector ToKAVector() => new KAVector(Vector);

        public KAQuaternion Clone() => new KAQuaternion(this);

        public static bool Equals(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => quaternionLeft.W == quaternionRight.W
            && quaternionLeft.Vector == quaternionRight.Vector;

        #region Operators

        public static KAQuaternion operator +(KAQuaternion quaternion, KAQuaternion p)
            => new KAQuaternion(quaternion.Vector + p.Vector, quaternion.W + p.W);

        public static KAQuaternion operator -(KAQuaternion quaternion, KAQuaternion p)
            => new KAQuaternion(quaternion.Vector - p.Vector, quaternion.W - p.W);

        public static KAQuaternion operator *(KAQuaternion quaternion, int c)
            => quaternion * (double)c;

        public static KAQuaternion operator *(int c, KAQuaternion quaternion)
            => quaternion * (double)c;

        public static KAQuaternion operator *(KAQuaternion quaternion, float c)
            => quaternion * (double)c;

        public static KAQuaternion operator *(float c, KAQuaternion quaternion)
            => quaternion * (double)c;

        public static KAQuaternion operator *(KAQuaternion quaternion, double c)
            => new KAQuaternion(quaternion.Vector * c, quaternion.W * c);

        public static KAQuaternion operator *(double c, KAQuaternion quaternion)
            => quaternion * c;

        public static KAQuaternion operator &(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => new KAQuaternion((quaternionRight.Vector * quaternionLeft.W)
                + (quaternionLeft.Vector * quaternionRight.W)
                + (quaternionLeft.Vector & quaternionRight.Vector),
                quaternionLeft.W * quaternionRight.W
                - (quaternionLeft.Vector * quaternionRight.Vector));

        public static KAQuaternion operator ^(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => quaternionRight & quaternionLeft;

        public static KAQuaternion operator /(KAQuaternion quaternion, double c)
            => new KAQuaternion(quaternion.Vector / c, quaternion.W / c);

        public static KAQuaternion operator /(KAQuaternion quaternion, float c)
            => quaternion / (double)c;

        public static KAQuaternion operator /(KAQuaternion quaternion, int c)
            => quaternion / (double)c;

        public static double operator *(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => (quaternionLeft.Vector * quaternionRight.Vector)
            + (quaternionLeft.W * quaternionRight.W);

        public static bool operator ==(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => quaternionLeft.Equals(quaternionRight);

        public static bool operator !=(KAQuaternion quaternionLeft, KAQuaternion quaternionRight)
            => !quaternionLeft.Equals(quaternionRight);

        public static KAQuaternion operator -(KAQuaternion quaternion)
            => quaternion * -1.0;

        public static explicit operator KAVector(KAQuaternion quaternion)
            => new KAVector(quaternion.Vector);

        #endregion
    }
}
