﻿using System;
using System.Text;

namespace A1i1uyamax
{
    [Serializable]
    public sealed class KAVector
    {
        public static readonly KAVector NULL = new KAVector(0.0, 0.0, 0.0);
        public static readonly KAVector Vi = new KAVector(1.0, 0.0, 0.0);
        public static readonly KAVector Vj = new KAVector(0.0, 1.0, 0.0);
        public static readonly KAVector Vk = new KAVector(0.0, 0.0, 1.0);

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public KAVector()
        {
        }

        public KAVector(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public KAVector(float X, float Y, float Z) : this((double)X, (double)Y, (double)Z)
        {
        }

        public KAVector(KAVector vector) : this(vector.X, vector.Y, vector.Z)
        {
        }

        public KAVector(KAQuaternion quaternion) : this(quaternion.Vector)
        {
        }

        public override bool Equals(object obj)
            => obj is KAVector vector && Equals(vector);

        public bool Equals(KAVector vector)
            => vector != null && Equals(this, vector);

        public override int GetHashCode()
            => X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();

        public double Module()
            => Math.Sqrt(X * X + Y * Y + Z * Z);

        public KAVector Normal()
            => Module() != 0.0 ? this / Module() : NULL;

        public override string ToString()
            => new StringBuilder()
                .Append($"{X} ")
                .Append($"{Y} ")
                .Append(Z)
                .ToString();

        public string ToString(string format)
            => new StringBuilder()
                .AppendFormat(format, $"{X} ")
                .AppendFormat(format, $"{Y} ")
                .AppendFormat(format, Z)
                .ToString();

        public KAQuaternion ToKAQuaternion()
            => new KAQuaternion(this);

        public KAMatrix ToKAMatrix() => new KAMatrix(3, 1)
        {
            [0, 0] = X,
            [1, 0] = Y,
            [2, 0] = Z
        };

        public KAVector Clone() => new KAVector(this);

        public static double DistanceSqr(KAVector vectorLeft, KAVector vectorRight)
        {
            double divX = vectorRight.X - vectorLeft.X;
            double divY = vectorRight.Y - vectorLeft.Y;
            double divZ = vectorRight.Z - vectorLeft.Z;
            return divX * divX + divY * divY + divZ * divZ;
        }

        public static double Distance(KAVector a, KAVector b)
            => Math.Sqrt(DistanceSqr(a, b));

        public static bool Equals(KAVector vectorLeft, KAVector vectorRight)
            => vectorLeft.X == vectorRight.X
            && vectorLeft.Y == vectorRight.Y
            && vectorLeft.Z == vectorRight.Z;

        #region Operators

        public static KAVector operator -(KAVector vector)
            => vector * -1.0;

        public static KAVector operator +(KAVector vectorLeft, KAVector vectorRight)
            => new KAVector(vectorLeft.X + vectorRight.X,
                            vectorLeft.Y + vectorRight.Y,
                            vectorLeft.Z + vectorRight.Z);

        public static KAVector operator -(KAVector vectorLeft, KAVector vectorRight)
            => new KAVector(vectorLeft.X - vectorRight.X,
                            vectorLeft.Y - vectorRight.Y,
                            vectorLeft.Z - vectorRight.Z);

        public static KAVector operator *(KAVector vector, int c)
            => vector * (double)c;

        public static KAVector operator *(int c, KAVector vector)
            => vector * (double)c;

        public static KAVector operator *(KAVector vector, float c)
            => vector * (double)c;

        public static KAVector operator *(float c, KAVector vector)
            => vector * (double)c;

        public static KAVector operator *(KAVector vector, double c)
            => new KAVector(c * vector.X, c * vector.Y, c * vector.Z);

        public static KAVector operator *(double c, KAVector vector)
            => vector * c;

        public static KAVector operator /(KAVector vector, int c)
            => vector / (double)c;

        public static KAVector operator /(KAVector vector, float c)
            => vector / (double)c;

        public static KAVector operator /(KAVector vector, double c)
            => new KAVector(vector.X / c, vector.Y / c, vector.Z / c);

        public static double operator *(KAVector a, KAVector b)
            => (a.X * b.X + a.Y * b.Y + a.Z * b.Z);

        public static KAVector operator &(KAVector a, KAVector b)
            => new KAVector((a.Y * b.Z) - (a.Z * b.Y),
                            (a.Z * b.X) - (a.X * b.Z),
                            (a.X * b.Y) - (a.Y * b.X));

        public static bool operator ==(KAVector a, KAVector b)
            => a.Equals(b);

        public static bool operator !=(KAVector a, KAVector b)
            => !a.Equals(b);

        public static bool operator <(KAVector a, KAVector b)
            => a.Module() < b.Module();

        public static bool operator >(KAVector a, KAVector b)
            => a.Module() > b.Module();

        #endregion
    }
}
