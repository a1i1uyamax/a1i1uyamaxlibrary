﻿using System;

namespace A1i1uyamax.Statistics
{
    public abstract class AbstractFilter
    {
        protected double dt = 0.075;
        protected double delta = 0.000175;
        protected KAMatrix matrK;
        protected readonly Func<KAMatrix, double, KAVector, KAMatrix> systemFunc;

        public double DiscreteTime => dt;
        public KAMatrix K => matrK;

        protected AbstractFilter(Func<KAMatrix, double, KAVector, KAMatrix> systemFunc)
        {
            this.systemFunc = systemFunc;
        }

        public abstract KAMatrix Step(KAMatrix vectorOfMeasurements,
            KAVector vectorOfControl = null,
            KAMatrix predictedVectorOfSystem = null);

        protected KAMatrix KAMatrixOfDerivatives(int rows, int columns,
            Func<KAMatrix, double, KAVector, KAMatrix> func,
            KAMatrix predictedVectorOfSystem, KAVector vectorOfControl = null)
        {
            KAMatrix xr, x0, x1, xf;
            var result = new KAMatrix(rows, columns);
            for (int i = 0; i < columns; i++)
            {
                xr = new KAMatrix(columns, 1);

                xr[i, 0] = delta;
                x0 = new KAMatrix(columns, 1);
                x0 = func(predictedVectorOfSystem + xr, dt,
                    vectorOfControl ?? KAVector.NULL);

                xr[i, 0] = -delta;
                x1 = new KAMatrix(columns, 1);
                x1 = func(predictedVectorOfSystem + xr, dt,
                    vectorOfControl ?? KAVector.NULL);

                xf = new KAMatrix(columns, 1);
                xf = (x0 - x1) / 2 / delta;

                for (int j = 0; j < rows; j++)
                    result[j, i] = xf[j, 0];
            }
            return result;
        }
    }
}