﻿using System;

namespace A1i1uyamax.Statistics
{
    public sealed class EKF : AbstractFilter
    {
        private KAMatrix matrQ;
        private KAMatrix matrR;
        private KAMatrix correctedP;
        private KAMatrix predictedP;
        private KAMatrix correctedX;
        private KAMatrix predictedX;
        private readonly Func<KAMatrix, double, KAVector, KAMatrix> mesurmentsFunc;

        public KAMatrix P => correctedP;

        private EKF(KAMatrix manualVectorOfSystem,
            KAMatrix manualCovarianceMatrix,
            int vectorOfMeasurementsRows,
            Func<KAMatrix, double, KAVector, KAMatrix> systemFunc,
            Func<KAMatrix, double, KAVector, KAMatrix> mesurementsFunc)
            : base(systemFunc)
        {
            predictedP = new KAMatrix(manualVectorOfSystem.Row, manualVectorOfSystem.Row);
            matrR = new KAMatrix(vectorOfMeasurementsRows, vectorOfMeasurementsRows);
            matrQ = new KAMatrix(manualVectorOfSystem.Row, manualVectorOfSystem.Row);
            matrK = new KAMatrix(manualVectorOfSystem.Row, vectorOfMeasurementsRows);
            this.mesurmentsFunc = mesurementsFunc;
            try
            {
                predictedX = correctedX = manualVectorOfSystem;
                correctedP = manualCovarianceMatrix;
            }
            catch (Exception ex)
            {
                throw new Exception("Incorrect arguments!", ex);
            }
        }

        public override KAMatrix Step(KAMatrix vectorOfMeasurements,
            KAVector vectorOfControl = null,
            KAMatrix predictedVectorOfSystem = null)
        {
            if (vectorOfMeasurements.Row != matrR.Row)
                throw new ArgumentException("Wrong measurements count!");

            if (vectorOfMeasurements.Column != 1)
                throw new ArgumentException("Vector of measurements row not equals to 1.");

            predictedX = predictedVectorOfSystem ?? systemFunc(correctedX, dt, vectorOfControl);

            var matrF = KAMatrixOfDerivatives(predictedX.Row, predictedX.Column,
                systemFunc, predictedX, vectorOfControl);
            predictedP = matrF * correctedP * matrF.Transpose() + matrQ;

            var matrH = KAMatrixOfDerivatives(vectorOfMeasurements.Row, vectorOfMeasurements.Column,
                mesurmentsFunc, predictedX, vectorOfControl);
            matrK = predictedP * matrH.Transpose() * (matrH * predictedP * matrH.Transpose()
                + matrR).Inverse(KAMatrix.InverseMethod.ByMinors);
            correctedX = predictedX + matrK * (vectorOfMeasurements - mesurmentsFunc(correctedX, dt, vectorOfControl));
            correctedP = (KAMatrix.I(predictedX.Row) - matrK * matrH) * predictedP;

            return correctedX;
        }

        public class EKFBuilder
        {
            private EKF ekf;

            public EKFBuilder(KAMatrix manualSystem, KAMatrix manualP, int measurementsVectorRow,
                Func<KAMatrix, double, KAVector, KAMatrix> systemFunc,
                Func<KAMatrix, double, KAVector, KAMatrix> mesurmentsFunc)
            {
                ekf = new EKF(manualSystem, manualP, measurementsVectorRow, systemFunc, mesurmentsFunc);
            }

            public EKFBuilder SetMatrixOfSystemNoize(KAMatrix matrixOfSystemNoize)
            {
                if (matrixOfSystemNoize.Row != ekf.matrQ.Row
                    || matrixOfSystemNoize.Column != ekf.matrQ.Column)
                    throw new ArgumentException("Incorrect size of matrix.",
                        nameof(matrixOfSystemNoize));

                ekf.matrQ = matrixOfSystemNoize;
                return this;
            }

            public EKFBuilder SetMatrixOfMeasurementsNoize(KAMatrix matrixOfMeasurementsNoize)
            {
                if (matrixOfMeasurementsNoize.Row != ekf.matrR.Row
                    || matrixOfMeasurementsNoize.Column != ekf.matrR.Column)
                    throw new ArgumentException("Incorrect size of matrix.",
                        nameof(matrixOfMeasurementsNoize));

                ekf.matrR = matrixOfMeasurementsNoize;
                return this;
            }

            public EKFBuilder SetDelta(double delta)
            {
                ekf.delta = delta;
                return this;
            }

            public EKFBuilder SetDiscreteTime(double dt)
            {
                ekf.dt = dt;
                return this;
            }

            public EKF Create() => ekf;
        }
    }
}
